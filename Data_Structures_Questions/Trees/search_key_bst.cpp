#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

bool is_tree_pres(tnode *root, int key) // preorder
{
 
 if (root->left != NULL && root->right != NULL)
    {    
     cout << root->data << " " << '\n';
      return ( (root->data == key) || is_tree_pres(root->left, key)
      ||  is_tree_pres(root->right, key) );
    }
    
    else if (root->left == NULL && root->right == NULL)
    { 
      cout << root->data << " " << '\n';       
     if (root->data == key)
      { return true; }

      else 
      { return false; }  
    }

    else if (root->left != NULL)
    {    
       cout << root->data << " " << '\n'; 
       return ( (root->data == key) || is_tree_pres(root->left, key) ); 
    }
    
    else if (root->right != NULL)
    {  
      cout << root->data << " " << '\n'; 
      return ( (root->data == key) || is_tree_pres(root->right, key) ); 
        
    }
    
}


int main()
{   
    

    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
    int el = 70;

    if (is_tree_pres(root,42))
    { cout << "yes"; }
    return 0;
}