#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;

class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};
int min_bst(tnode *root)
{
    if (root->left != NULL)
        {return min_bst(root->left); }

    if (root->left == NULL)
        return root->data;
}

int max_bst(tnode *root)
{
    if (root->right != NULL)
        {return max_bst(root->right); }

    if (root->right == NULL)
        return root->data;
}
bool is_tree_bst(tnode *root)
{
    if (root->left != NULL && root->right != NULL) // both children 
    {
        return root->data > max_bst(root->left) 
        && root->data <= min_bst(root->right)
        && is_tree_bst(root->left) && is_tree_bst(root->right);
    }

    else if (root->left != NULL && root->right == NULL) // only left child 
    {
     return root->data > max_bst(root->left) && is_tree_bst(root->left);
    }
    
    else if (root->left == NULL && root->right != NULL) // only right child 
    {
     return root->data <= min_bst(root->right) && is_tree_bst(root->right);
    }

    else if (root->left == NULL && root->right == NULL) // no children 
    {
     return true;
    }
}
int main()
{
    
    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
   if (is_tree_bst(root))
   { cout << "true";}
   
    return 0;

}