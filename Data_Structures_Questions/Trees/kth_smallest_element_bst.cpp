#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};


void kth_smallest(tnode *root, int &wh, int k) 
{
   if (root->left != NULL && root->right != NULL)
    { 
     
     
     kth_smallest(root->left, wh, k);
     wh++;
      if (wh == k)
      {  cout << root->data << " "; }
     kth_smallest(root->right, wh, k);
    
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
        wh++;
         if (wh == k)
      {  cout << root->data << " "; }
     return;
    }

    else if (root->left != NULL)
    {
        
     kth_smallest(root->left, wh, k); 
     wh++;    
      if (wh == k)
      {  cout << root->data << " "; }
     
    }
    
    else if (root->right != NULL)
    {
        wh++;   
         if (wh == k)
      {  cout << root->data << " "; }
     kth_smallest(root->right, wh, k);
     
    }
    
    
    
}

int main()
{
    
    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    int c = 0;
    kth_smallest(root, c, 11); 
    return 0;

}