#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		char data;
		tnode *left;
		tnode *right;
		tnode (char d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

bool are_trees_same(tnode *root, tnode *root2) // inorder
{
 
 if (root->left != NULL && root->right != NULL
     && root2->left != NULL && root2->right != NULL)
    {    
          
       
       return are_trees_same(root->left, root2->left) && (root->data == root2->data)
       && are_trees_same(root->right, root2->right);
       
       
    }
    
    else if (root->left == NULL && root->right == NULL
             && root2->left == NULL && root2->right == NULL)
    {    
        
        return (root->data == root2->data);
    }

    else if (root->left != NULL && root2->left != NULL)
    {    
        
        return are_trees_same(root->left, root2->left) && (root->data == root2->data);  
    }
    
    else if (root->right != NULL && root2->right != NULL)
    {   
    
      return (root->data == root2->data) && are_trees_same(root->right, root2->right);
        
    }

};

int main()
{   
    

    tnode *root = new tnode('A');
    
    root->right = new tnode('E');
    
    root->left = new tnode('B');
    
    root->left->left = new tnode('C');
    root->left->right = new tnode('D');
    root->right->right = new tnode('G');
    root->right->left = new tnode('F');

    root->left->left->left = new tnode('I');
    root->left->right->right = new tnode('X');
    root->left->left->right = new tnode('J');

    root->left->right->right->left = new tnode('K');
    
    
tnode *root2 = new tnode('A');
root2->right = new tnode('E');
root2->left = new tnode('B');
    
  root2->left->left = new tnode('C');
    root2->left->right = new tnode('D');
    root2->right->right = new tnode('G');
    root2->right->left = new tnode('F');

    root2->left->left->left = new tnode('I');
    root2->left->right->right = new tnode('X');
    root2->left->left->right = new tnode('J');

    root2->left->right->right->left = new tnode('K');
    if (are_trees_same(root,root2))
    { cout << "The trees are the same ";}
    return 0;
}