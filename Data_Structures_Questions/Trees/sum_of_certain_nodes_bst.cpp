#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

int sum_certain_nodes(tnode *root, int di) // di is the element we shouldn't include  
{                                          // di is the data stored in a certain node 
   if (root->left != NULL && root->right != NULL)
    { 
     
    if (root->data != di)
     {
     return sum_certain_nodes(root->left, di)
     + root->data + 
     sum_certain_nodes(root->right, di);
     }

     else if (root->data == di)
     {
     return sum_certain_nodes(root->left, di)
     + 0 + 
     sum_certain_nodes(root->right, di); 
     }
    
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
       if (root->data != di)
         { return root->data;}
       else if (root->data == di) 
         { return 0; }  
    }

    else if (root->left != NULL)
    {

    if (root->data != di)
     { return sum_certain_nodes(root->left, di) + root->data; }

     else if (root->data == di)
     { return sum_certain_nodes(root->left, di) + 0;} 
     
    }
    
    else if (root->right != NULL)
    {

     if (root->data != di)
     { return root->data + sum_certain_nodes(root->right, di); }

     else if (root->data == di)
     { return 0 + sum_certain_nodes(root->right, di);} 
     
    }
     
    
}

int main()
{
    
    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    cout << sum_certain_nodes(root, 150);
    return 0;

}