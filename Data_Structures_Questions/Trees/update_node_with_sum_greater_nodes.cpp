#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

int sum_greater_nodes(tnode *root, int di) // di is the element we shouldn't include  
{                                          // di is the data stored in a certain node 
                                          // returns the value of all the greater nodes 
   if (root->left != NULL && root->right != NULL)
    { 
     
    if (root->data > di)
     {
     return sum_greater_nodes(root->left, di)
     + root->data + 
     sum_greater_nodes(root->right, di);
     }

     else if (root->data <= di)
     {
     return sum_greater_nodes(root->left, di)
     + 0 + 
     sum_greater_nodes(root->right, di); 
     }
    
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
       if (root->data > di)
         { return root->data;}
       else if (root->data <= di) 
         { return 0; }  
    }

    else if (root->left != NULL)
    {

    if (root->data > di)
     { return sum_greater_nodes(root->left, di) + root->data; }

     else if (root->data <= di)
     { return sum_greater_nodes(root->left, di) + 0;} 
     
    }
    
    else if (root->right != NULL)
    {

     if (root->data > di)
     { return root->data + sum_greater_nodes(root->right, di); }

     else if (root->data <= di)
     { return 0 + sum_greater_nodes(root->right, di);} 
     
    }
     
    
}

void rep_node_with_greater(tnode *root, tnode *root_cpy) // this function replaces node with the sum of all the other nodes 
{
   if (root->left != NULL && root->right != NULL)
    { 
     
     
     rep_node_with_greater(root->left, root_cpy);
     root->data = sum_greater_nodes(root_cpy, root->data);
     cout << root->data << " ";
     rep_node_with_greater(root->right, root_cpy);
    
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
     root->data = sum_greater_nodes(root_cpy, root->data);
     cout << root->data << " ";  
     return;
    }

    else if (root->left != NULL)
    {    
     rep_node_with_greater(root->left, root_cpy); 
     root->data = sum_greater_nodes(root_cpy, root->data);
     cout << root->data << " ";
    }
    
    else if (root->right != NULL)
    {
     root->data = sum_greater_nodes(root_cpy, root->data);
     cout << root->data << " ";
     rep_node_with_greater(root->right, root_cpy);
    }
    
    
    
}
int main()
{
    
    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);


    
    tnode *root2 = new tnode(100);
    
    root2->right = new tnode(150);
    
    root2->left = new tnode(50);
    
    root2->left->left = new tnode(35);
    root2->left->right = new tnode(60);
    root2->right->right = new tnode(170);
    root2->right->left = new tnode(130);

    root2->left->left->left = new tnode(25);
    root2->left->right->right = new tnode(70);
    root2->left->left->right = new tnode(42);

    root2->left->right->right->left = new tnode(65);
    
  
  rep_node_with_greater(root, root2);
    return 0;

}