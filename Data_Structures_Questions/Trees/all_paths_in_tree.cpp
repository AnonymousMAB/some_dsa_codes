#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		char data;
		tnode *left;
		tnode *right;
		tnode (char d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

bool is_in(vector <char> ar, char el)
{
    for (int counter = 0; counter < ar.size(); counter++)
    {
        if (ar[counter] == el)
        {
            return true;
        }
    }
   return false; 
}

int count_nodes(tnode *root) // preorder
{
 
 if (root->left != NULL && root->right != NULL)
    {    
       
        return 1 + (count_nodes(root->left) + 
        
        count_nodes(root->right));   
       
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
        return 1;
    }

    else if (root->left != NULL)
    {    
     return 1 + count_nodes(root->left);  
    }
    
    else if (root->right != NULL)
    {   
      return 1 +  count_nodes(root->right);
    }

}

void fin_paths(tnode *root, vector<char> sta,  vector<char> alr_in)
{
    if (root->left != NULL && root->right != NULL)
    {    
          sta.push_back(root->data);

        if (!is_in(alr_in, root->data))
         {  alr_in.push_back(root->data); 
            
            for (int counter = 0; counter < sta.size(); counter++)
            {
                cout << sta[counter] << " ";
            }
           cout << '\n'; 
         
         }
         
         
              
       
         
        fin_paths(root->left, sta, alr_in);
        //sta.erase(sta.begin() + sta.size() - 1);
        
        for (;sta[sta.size() - 1] != root->data;)
            { sta.erase(sta.begin() + sta.size() - 1);  }
            
        fin_paths(root->right, sta, alr_in);
    }
    
    else if (root->left == NULL && root->right == NULL)
    {     
   
          sta.push_back(root->data);
         if (!is_in(alr_in, root->data))
         {  alr_in.push_back(root->data); 
            
            for (int counter = 0; counter < sta.size(); counter++)
            {
                cout << sta[counter] << " ";
            }
           cout << '\n'; 
         
         } 

        

          sta.erase(sta.begin() + sta.size() - 1);
          
        return;
    }

    else if (root->left != NULL)
    {    
          sta.push_back(root->data);
         if (!is_in(alr_in, root->data))
         {  alr_in.push_back(root->data); 
            
            for (int counter = 0; counter < sta.size(); counter++)
            {
                cout << sta[counter] << " ";
            }
           cout << '\n'; 
         
         }

       
        fin_paths(root->left, sta, alr_in);
        //sta.erase(sta.begin() + sta.size() - 1);
    }
    
    else if (root->right != NULL)
    {   
          sta.push_back(root->data);
         if (!is_in(alr_in, root->data))
         {  alr_in.push_back(root->data); 
            
            for (int counter = 0; counter < sta.size(); counter++)
            {
                cout << sta[counter] << " ";
            }
           cout << '\n'; 
         
         } 
        
        
        fin_paths(root->right, sta, alr_in);
        //sta.erase (sta.begin() + sta.size() - 1);
    }
   
  
 

};

int main()
{   
    

    tnode *root = new tnode('A');
    
    root->right = new tnode('E');
    
    root->left = new tnode('B');
    
    root->left->left = new tnode('C');
    root->left->right = new tnode('D');
    root->right->right = new tnode('G');
    root->right->left = new tnode('F');

    root->left->left->left = new tnode('I');
    root->left->right->right = new tnode('X');
    root->left->left->right = new tnode('J');

    root->left->right->right->left = new tnode('K');
    
    

    vector <char> alr_in;
    vector<vector<char>> all_paths;
    vector <char> sta;

    fin_paths(root, sta, alr_in);
    return 0;
}
