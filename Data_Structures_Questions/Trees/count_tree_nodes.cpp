#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

int count_nodes(tnode *root) // preorder
{
 
 if (root->left != NULL && root->right != NULL)
    {    
       
        return 1 + (count_nodes(root->left) + 
        
        count_nodes(root->right));   
       
    }
    
    else if (root->left == NULL && root->right == NULL)
    {  
        return 1;
    }

    else if (root->left != NULL)
    {    
     return 1 + count_nodes(root->left);  
    }
    
    else if (root->right != NULL)
    {   
      return 1 +  count_nodes(root->right);
    }

}



int main()
{   
    

    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
    int el = 70;

  cout << count_nodes(root);
    return 0;
}

