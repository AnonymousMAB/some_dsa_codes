#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>
// here we'll be finding whether a binary tree is part of a larger binary tree 
using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};
 //cout << "root->data, root2->data :: " << root->data << " " << hd << '\n';

bool tree_subq(tnode *root, tnode *root2) // only used this when you have already traversed the larger
{                                         // subtree to find the common head data  
 // root2 is the smaller binary tree 
 if (root2->left != NULL && root2->right != NULL)
    {    
       return tree_subq(root->left, root2->left) && (root->data == root2->data)
       && tree_subq(root->right, root2->right);    
    }
    
    else if (root2->left == NULL && root2->right == NULL)
    {     
        return (root->data == root2->data);
    }

    else if (root2->left != NULL)
    {    
        return tree_subq(root->left, root2->left) && (root->data == root2->data);  
    }
    
    else if (root2->right != NULL)
    {   
      return (root->data == root2->data) && tree_subq(root->right, root2->right); 
    }

};



void is_subtree(tnode *root, tnode *root2, int hd) // hd is the data of the head of the 2nd smaller 
                                                    // tree 
{  // root2 is the smaller binary tree
 
 if (root->left != NULL && root->right != NULL)
    {     
        is_subtree(root->left, root2, hd);
       // cout << "root->data, hd :: " << root->data << " " << hd << '\n';
      if (root->data == hd)
       {
           if (tree_subq(root, root2))
           { cout << "Tree2 is a subtree " << '\n';}
       }

        is_subtree(root->right, root2, hd);
       
    }
    
    else if (root->left == NULL && root->right == NULL)
    {    
      
        if (root->data == hd)
        { cout << "Tree2 is a subtree " << '\n';}
    }

    else if (root->left != NULL)
    {    
       
        is_subtree(root->left, root2, hd);  
       
      if (root->data == hd)
       {
           if (tree_subq(root, root2))
           { cout << "Tree2 is a subtree " << '\n';}
       }
    }
    
    else if (root->right != NULL)
    { 
       
      if (root->data == hd)
       {
           if (tree_subq(root, root2))
           { cout << "Tree2 is a subtree " << '\n';}
       }
      
      is_subtree(root->right, root2, hd);
        
    }

};



int main()
{   
    

    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
    
    
tnode *root2 = new tnode(100);
root2->right = new tnode(150);
root2->left = new tnode(50);
root2->left->left = new tnode(35);
root2->left->left->right = new tnode(42);
    int hd = 100;

    is_subtree(root, root2, hd);

    
   
  
    return 0;
}