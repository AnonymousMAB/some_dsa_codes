#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std; 

void inorder(vector<int> ar, int index) // inorder using an array 
{
    if (index >= ar.size() || ar[index] == -123)
    {   
        
        return;
    }
   
    inorder(ar, 2*index);
    cout << ar[index] << " ";
    inorder(ar, 2*index+1);
}

bool does_el_exist(vector<int> ar, int index, int el)
{
    if (index >= ar.size() || ar[index] == -123)
    {   
        return false;
    }

    return does_el_exist(ar, 2*index, el) || (ar[index]==el) || does_el_exist(ar, 2*index + 1, el);
}


int main()
{
    vector<int> ar;
    vector<int> ar2;
    ar.push_back(-123);
    ar.push_back(100);
    ar.push_back(50);
    ar.push_back(150);
    ar.push_back(35);
    ar.push_back(60);
    ar.push_back(130);
    ar.push_back(170);
    ar.push_back(25);
    ar.push_back(42);
    ar.push_back(-123);
    ar.push_back(70);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(-123);
    ar.push_back(65);
    
    
    return 0;
}