#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		char data;
		tnode *left;
		tnode *right;
		tnode (char d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};

int max(int num1, int num2)
{
    if (num1 >= num2)
    return num1;

    else 
    return num2;
}

int height(tnode *root) 
{
 
 if (root->left != NULL && root->right != NULL)
    {    
          return 1 + max( height(root->left), height(root->right) );
    }
    
    else if (root->left == NULL && root->right == NULL)
    {    
      return 0; 
    }

    else if (root->left != NULL)
    {    
       return 1 +  height(root->left);
    }
    
    else if (root->right != NULL)
    {   
       return 1 +  height(root->right);
    }

};


int main()
{   
    

    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->left->left->left = new tnode(123);
    root->left->left->left->left->right = new tnode(123);
    root->left->left->left->left->right->right = new tnode(1231);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
   root->left->right->right->left->right = new tnode(612);
  cout << height(root);
    return 0;
}

