#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};
// output in preorder 
 bool is_in(vector<int> ar, int el)
 {
    for (int counter = 0; counter < ar.size(); counter++)
    {
        if (ar[counter] == el)
        {
            return true;
        }
    }
   return false; 
 }

 int ret_root(vector<int> postorder, vector<int> arr) // returns root from a subtree 
 {
  vector<int> temp;

  for (int counter = 0; counter < postorder.size(); counter++)
  {
      if (is_in(arr, postorder[counter]))
        { temp.push_back(postorder[counter]); }
  }
  return temp[temp.size() - 1];
 
 }
void cons_tree(vector<int> inorder, vector<int> postorder, vector<int> lft, vector<int> rght, int initial)
{
// lft is the array which contains the left subtree values, rght contains right subtree values
// initial is for telling us whether we've called the function for the very first time 

 if (initial == 0)
 {
  int root = postorder[postorder.size() - 1];
  cout << root << " ";
  int counter;
  for (counter = 0; inorder[counter] != root; counter++)
  {
    lft.push_back(inorder[counter]); // right subtree 
  }
  counter++;

  for (; counter < inorder.size(); counter++)
  {
      rght.push_back(inorder[counter]); // left subtree 
  }

  cons_tree(inorder,  postorder, lft, rght, 1); 
 }

 else if(lft.size() > 0 && rght.size() > 0) // both left and right subtree present 
 {
  int root;
  root = ret_root(postorder, lft);
  cout << root << " ";
  int counter;
  vector<int> l, r; // left right subtree in the original subtree 

  for (counter = 0; inorder[counter] != root; counter++) // left from left 
  {
    if (is_in(lft, inorder[counter]))
      { l.push_back(inorder[counter]); } 
  }
  counter++; 
  
  for (; counter < inorder.size(); counter++) // right from left 
  {
    if (is_in(lft, inorder[counter]))
      { r.push_back(inorder[counter]); } 
  }

  cons_tree(inorder,  postorder, l, r, 1);

  counter = 0;
  l.clear();
  r.clear();
 
  root = ret_root(postorder, rght);
  cout << root << " ";
  
  for (counter = 0; inorder[counter] != root; counter++) // left from right 
  {
    if (is_in(rght, inorder[counter]))
      { l.push_back(inorder[counter]); } 
  }
  counter++; 
  
  for (; counter < inorder.size(); counter++) // right from right 
  {
    if (is_in(rght, inorder[counter]))
      { r.push_back(inorder[counter]); } 
  }

  cons_tree(inorder,  postorder, l, r, 1);
 }

 else if(lft.size() > 0 && rght.size() == 0) // only left subtree present 
 { // use this as a template 
  int root;
  root = ret_root(postorder, lft);
  cout << root << " ";
  int counter;
  vector<int> l, r; // left right subtree in the original subtree 

  for (counter = 0; inorder[counter] != root; counter++) // left from left 
  {
    if (is_in(lft, inorder[counter]))
      { l.push_back(inorder[counter]); } 
  }
  counter++; 
  
  for (; counter < inorder.size(); counter++) // right from left 
  {
    if (is_in(lft, inorder[counter]))
      { r.push_back(inorder[counter]); } 
  }

  cons_tree(inorder,  postorder, l, r, 1);

 }

 else if(lft.size() == 0 && rght.size() > 0) // only right subtree present 
 {
  int root;
  root = ret_root(postorder, rght);
  cout << root << " ";
  int counter;
  vector<int> l, r; // left right subtree in the original subtree 

  for (counter = 0; inorder[counter] != root; counter++) // left from right 
  {
    if (is_in(rght, inorder[counter]))
      { l.push_back(inorder[counter]); } 
  }
  counter++; 
  
  for (; counter < inorder.size(); counter++) // right from right 
  {
    if (is_in(rght, inorder[counter]))
      { r.push_back(inorder[counter]); } 
  }

  cons_tree(inorder,  postorder, l, r, 1);
 }

 else if(lft.size() == 0 && rght.size() == 0) // no subtree present 
 { return; }

}

int main()
{
    
    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
    root->left = new tnode(50);
    
    root->left->left = new tnode(35);
    root->left->right = new tnode(60);
    root->right->right = new tnode(170);
    root->right->left = new tnode(130);

    root->left->left->left = new tnode(25);
    root->left->right->right = new tnode(70);
    root->left->left->right = new tnode(42);

    root->left->right->right->left = new tnode(65);
    
    vector<int> inorder;
    vector<int> postorder;

    inorder.push_back(25);
    inorder.push_back(35);
    inorder.push_back(42);
    inorder.push_back(50);
    inorder.push_back(60);
    inorder.push_back(65);
    inorder.push_back(70);
    inorder.push_back(100);
    inorder.push_back(130);
    inorder.push_back(150);
    inorder.push_back(170);

    postorder.push_back(25);
    postorder.push_back(42);
    postorder.push_back(35);
    postorder.push_back(65);
    postorder.push_back(70);
    postorder.push_back(60);
    postorder.push_back(50);
    postorder.push_back(130);
    postorder.push_back(170);
    postorder.push_back(150);
    postorder.push_back(100);
   
    vector<int> lft, rght; 
    cons_tree(inorder, postorder, lft, rght, 0);
    
    // output of tree is in preorder 
    return 0;
}