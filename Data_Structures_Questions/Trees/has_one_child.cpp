#include <iostream> 
#include <vector> 
#include <string>
#include <stdlib.h>
#include <algorithm>
#include <cstdlib>
#include <ctime>

using namespace std;
class tnode{
	public:
		int data;
		tnode *left;
		tnode *right;
		tnode (int d, tnode *n=NULL, tnode *n1=NULL)
        {
			data = d;
			left = n;
            right = n1;
		}
};


bool has_one_child(tnode *root) // tells us if all nodes of a binary tree have only one child 
{
 
 if (root->left != NULL && root->right != NULL)
    {    
          return false; 
    }
    
    else if (root->left == NULL && root->right == NULL)
    {    
        return true;
    }

    else if (root->left != NULL)
    {    
        return true && has_one_child(root->left);  
    }
    
    else if (root->right != NULL)
    {   
      return true && has_one_child(root->right);
    }

};

int main()
{   
    

    tnode *root = new tnode(100);
    
    root->right = new tnode(150);
    
   
    root->right->right = new tnode(170);
    root->right->right->right = new tnode (120);
    root->right->right->left = new tnode (324);
   
    
    if (has_one_child(root))
     { cout << "All nodes have only one child "; }

    else 
     { cout << "Not all nodes have one child ";  } 

    return 0;
}