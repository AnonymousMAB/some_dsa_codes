#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>


class node{
	public:
		int data;
		node *next;//TO point to next node, null otherwise
		//Constructor
		node (int d, node *n=NULL){
			data = d;
			next = n;
		}
};


// 1->3->5->7->9->x
bool check_all_neg(node *h, node *st)
{
    h = st;
    int neg = 0;
    int pos = 0;
    for (; h != NULL ; h = h->next)
    {
        if (h->data >= 0)
           { pos++ ;}

         if (h->data < 0)
           { neg++ ;}

    }

    if (pos == 0)
    { return true; }

    else 
    { return false; }
}
int main()
{
 
 node *h = new node(-11);

 node *st = h;

 h->next = new node(-3);

 h->next->next = new node(-5);

 h->next->next->next = new node(-7);

 h->next->next->next->next = new node(-9);

 h->next->next->next->next->next = NULL;
 
 int counter = 0;
 
 if (check_all_neg(h, st))
  { 
    cout << "The Linked List contains all negtaive numbers ";
    return 0;
  }
 h = st;
 
 for ( ;h != NULL ; h=h->next)
 {
     if (counter == 0 && h->data < 0)
     {
         while (h->data < 0 && h->next != NULL)
         {
             h = h->next;
             st = h;
         }

        counter++; 
     }

     if (h->next != NULL && h->next->data < 0)
     {
         while (h->next->data < 0 && h->next->next != NULL)
         {
             h->next = h->next->next;
         }
                  
     }

     if (h->next->data < 0 && h->next->next == NULL)
     {
         h->next = NULL;
     }
 }

 h=st;

 for (;h != NULL ; h=h->next)
 {
     cout << h->data << " ";
 }
 
 
    return 0;
}