#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>


class node{
	public:
		int data;
		node *next;//TO point to next node, null otherwise
		//Constructor
		node (int d, node *n=NULL){
			data = d;
			next = n;
		}
};

 bool are_lists_same(node *h1, node *st1, node *h2, node *st2)// tells us whether 2 linked lists are the same
 {
   h1 = st1;
   h2 = st2;
   
   for ( ; h1 != NULL; h1 = h1->next, h2 = h2->next)
   {
       if (h1->data != h2->data)
           return false;

       if (h2 == NULL)
           return false;    
   }

   if (h2 != NULL)
       return false;
    else
       return true;   
       
   
 }

// 1->3->5->7->9->x

int main()
{
 
 node *h = new node(-11);

 node *st = h;

 h->next = new node(-3);

 h->next->next = new node(5);

 h->next->next->next = new node(-7);

 h->next->next->next->next = new node(-9);

 h->next->next->next->next->next = NULL;


 node *h2 = new node(999);
 
 node *st2 = h2;

 h2->next = new node(123);

 h2->next->next = new node(876);

 h2->next->next->next = NULL;

 

 h = st;
if (are_lists_same(h, st, h2, st2))
{
    cout << "Lists are same";
}

else
{
    cout << "Lists aren't same";
}
    return 0;
}