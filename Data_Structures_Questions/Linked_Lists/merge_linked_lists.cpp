#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>

class node{
	public:
		int data;
		node *next;//TO point to next node, null otherwise
		//Constructor
		node (int d, node *n=NULL){
			data = d;
			next = n;
		}
};

// 1->3->5->7->9->x

int main()
{
 
 node *h = new node(-11);

 node *st = h;

 h->next = new node(-3);

 h->next->next = new node(5);

 h->next->next->next = new node(-7);

 h->next->next->next->next = new node(-9);

 h->next->next->next->next->next = NULL;


 node *h2 = new node(999);
 
 node *st2 = h2;

 h2->next = new node(123);

 h2->next->next = new node(876);

 h2->next->next->next = NULL;

 for (; h->next != NULL; h = h->next)
       { ; }
    
 for (; h2->next != NULL; h2 = h2->next)   
 {
     h->next = h2;
     h = h->next; 
 }

 h->next = h2;
 h->next->next = NULL;

 h = st;

 for (; h != NULL; h = h->next)
       { cout << h->data << " "; }
    return 0;
}