#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>

class node{
	public:
		int data;
		node *next;//TO point to next node, null otherwise
		//Constructor
		node (int d, node *n=NULL){
			data = d;
			next = n;
		}
};

// 1->3->5->7->9->x

int main()
{
 
 node *h = new node(-11);

 node *st = h;

 h->next = new node(-3);

 h->next->next = new node(5);

 h->next->next->next = new node(-7);

 h->next->next->next->next = new node(-9);

 h->next->next->next->next->next = NULL;
int t = 5; 
int pos = 0;
node *cpy;
 for (; h != NULL; h = h->next)
 {
    pos++;

   if (pos == 4)
   {   
       cpy = h->next->next;
       h->next->next = new node(100);
       h->next->next->next = cpy;
   }
 }
 h = st;
 for (; h != NULL; h = h->next)
 {
     cout << h->data << " ";
 }

    return 0;
}