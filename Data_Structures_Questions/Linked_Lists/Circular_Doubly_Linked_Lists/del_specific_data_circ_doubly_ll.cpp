#include <iostream>
using namespace std;
#include <ctime>
#include <string>
#include <stdlib.h>
#include <vector>
#include <algorithm>
class node{
	public:
		int data;
		node *next;//TO point to next node, null otherwise
        node *prev;
		//Constructor
		node (int d, node *n=NULL, node *n2 = NULL){
			data = d;
			next = n;
            prev = n2;
		}
};

void delete_node(node *h, node *st, int num)
{
    h = st;
    int pos = 1;
    int count = 0;
    for (; count != 1; h = h->next)
    {
        if (pos == num && h == st && pos == 1)
        {
            st = h->next;
            h->prev->next = h->next;
            h->next->prev = h->prev;
            break;
        }

        if (pos == num && pos != 1)
        {
            h->prev->next = h->next;
            h->next->prev = h->prev;
            break;
        }

        if (h->next == st)
        {
            count = 1;
            break;
        }
        
        pos++;
    }
    
    h = st;
    
    for (; true ; )
    {   
        
        cout << h->data << " ";
        h=h->next;
        
        if (h == st)
           {  break; }
    }
}
int main()
{

    node *h;
    h = new node(1);
    node *st = h;
    //h->next = new node(3);
    //h->next->next = h;
    //h->prev = h->next;
    //h->next->prev = h;

    h->next = new node(2); 
    h->next->next = new node(3);
    h->next->next->next = h;
    h->next->next->prev = h->next;
    h->next->prev = h;
    h->prev = h->next->next;
    
    // using 1 based indexing 

    
    int num; // this is the number we want to delete
    cin >> num; // the position we want to delete
    
    
    delete_node(h, st, num);

    return 0;
}